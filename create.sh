#!/usr/bin/env bash
set -e

echo "Create base terraform templates"
touch main.tf data.tf outputs.tf

if [ ! -f providers.tf ]
then
  echo "Create providers file"
  cat > providers.tf <<- PROVIDERS
provider "aws" {
  default_tags {
    tags = {
      created_by = "terraform"
      workspace = terraform.workspace
    }
  }
}
PROVIDERS
fi

if [ ! -f variables.tf ]
then
  echo "Create variables file"
  cat > variables.tf <<- VARIABLES
VARIABLES
fi

if [ ! -f locals.tf ]
then
  echo "Create locals template file"
  cat > locals.tf <<- LOCALS
locals {
}
LOCALS
fi

echo "Create default terraform variable files"
mkdir -p tfvars && touch tfvars/main.tfvars.json && touch tfvars/example.tfvars.json && touch tfvars/.gitkeep

echo "Create default backends directory"
mkdir -p tfbackend && touch tfbackend/.gitkeep

echo "Create .gitignore file"
cat > .gitignore <<- IGNORE
**/.terraform/**
**/.terragrunt-cache/**
.terraform.lock.hcl
main.tfvars.json
*.tfbackend
IGNORE
